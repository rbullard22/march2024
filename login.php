<?php
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Assuming form submission with username and password
    $username = $_POST['username'];
    $password = $_POST['password'];

    // Validate credentials (in a real scenario, you'd compare against a database)
    if ($username === 'user' && $password === 'password') {
        $_SESSION['loggedin'] = true;
        $_SESSION['username'] = $username;
        header("Location: secure_page.php");
        exit();
    } else {
        $error = "Invalid username or password.";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
</head>
<body>
    <h2>Login</h2>
    <?php if(isset($error)) { echo $error; } ?>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        Username: <input type="text" name="username"><br>
        Password: <input type="password" name="password"><br>
        <input type="submit" value="Login">
    </form>
</body>
</html>

<?php
session_start();

// Database connection parameters
$dbhost = 'localhost';  // Docker container's IP address or hostname
$dbport = '5432';       // Port mapped to the host machine
$dbname = 'postgres';
$dbuser = 'postgres';
$dbpass = 'mysecretpassword';

try {
    // Connect to PostgreSQL database using PDO
    $dsn = "pgsql:host=$dbhost;port=$dbport;dbname=$dbname;user=$dbuser;password=$dbpass";
    $dbconn = new PDO($dsn);

    // Set PDO to throw exceptions on error
    $dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    // Handle database connection errors
    echo "Failed to connect to the database: " . $e->getMessage();
    exit;
}

// Check if the form was submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve username and password from the form
    $usernameDb = $_POST['username'];
    $passwordDb = $_POST['password'];

    // Debugging
    echo "Username from form: $usernameDb<br>";
    echo "Password from form: $passwordDb<br>";

    try {
        // Prepare SQL statement to fetch user from the database
        $stmt = $dbconn->prepare("SELECT * FROM users WHERE username=:username");
        $stmt->bindParam(':username', $usernameDb);
        $stmt->execute();
        
        // Debugging
        echo "SQL query: SELECT * FROM users WHERE username='$usernameDb'<br>";

        // Fetch the result
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        var_dump($row); // Output the fetched row for debugging purposes

        // Check if the user exists and password is correct
        if ($row && $row['password'] == $passwordDb) {
            // Password is correct, set session variables
            $_SESSION['loggedin'] = true;
            $_SESSION['username'] = $usernameDb;
            // Redirect to secure page after successful login
            header("Location: secure_page.php");
            exit;
        } else {
            // Invalid username or password
            $error = "Invalid username or password.";
        }
    } catch (PDOException $e) {
        // Handle database query errors
        echo "Error in query: " . $e->getMessage();
        exit;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
</head>
<body>
    <h2>Login</h2>
    <?php if(isset($error)) { echo $error; } ?>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        Username: <input type="text" name="username"><br>
        Password: <input type="password" name="password"><br>
        <input type="submit" value="Login">
    </form>
</body>
</html>
